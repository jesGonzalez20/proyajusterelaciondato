#include <iostream>
#include <vector>
#include <stdlib.h>
#include <time.h>

#include <iomanip> 
#include <math.h> 

#include <sstream>
#include <string>
#include <fstream>

using namespace std;


const int poblacion_max   = 100;
const int poblacion_ini   = 5; 
int poblacion_act         = poblacion_ini;
int poblacion_act_des     = poblacion_act;
const int tam_individuo        = 13; //hacer calculo para generar el tama�o de genes

const double prob_mut     = 0.33;
const double prob_cru     = 0.7;

const int num_iteraciones = 10;

const int num_datos       = 40000;
const int num_sen         = 4;

//CLASE POBLACION
class Poblacion
{
	public:
		vector<vector<double> > poblacion;
	
	public:
		Poblacion();
		void poblar();
		double generarGen();
		void agregarIndividuo(vector<double>);
		void mostrarPoblacion(long double [][2]);
		int getSizePoblacion();		
};
Poblacion::Poblacion()
{
}
void Poblacion::poblar()
{
	for ( int i = 0; i < poblacion_max; i++ )
	{
		vector<double> individuo;
		for ( int j = 0; j < tam_individuo; j++ )
			individuo.push_back(generarGen());
			
		agregarIndividuo(individuo);
	}
}
double Poblacion::generarGen()
{
	return ((rand () % 2001) - 1000)/1000.0;
}
void Poblacion::agregarIndividuo(vector<double> individuo)
{
	poblacion.push_back(individuo);
}
int Poblacion::getSizePoblacion(){
	return poblacion.size();
}
void Poblacion::mostrarPoblacion(long double fitness[poblacion_max][2]){
	for ( int i = 0; i < poblacion_act; i++  )
	{
		for ( int j = 0; j < tam_individuo; j++ )
		{
			cout<<poblacion[i][j]<<" ";
		}
		cout<<"F= "<<fitness[i][0];
		cout<<endl;
	}
}

//CLASE DATASET
class Dataset
{
	public:
		double data[num_datos][num_sen];
		
	public:
		Dataset();
		void getDataset();
		void buildDataset(fstream &file,int indice);
};
Dataset::Dataset()
{
}
void Dataset::getDataset()
{
	//SE AGREGAN VALORES TEMPORALES EN DATASET SE AGREGARAN VALORES REALES DE LAS SE�ALES
	string radiacionSolar = "radiacion_solar.csv";
	string humedadRelativa = "humedad_relativa.csv";
	string temperaturaHambiente = "temperatura_ambiente.csv";
	string velocidadViento      = "velocidad_viento.csv";
	
	fstream rs(radiacionSolar.c_str());
	fstream ta(temperaturaHambiente.c_str());
	fstream hr(humedadRelativa.c_str());
	fstream vv(velocidadViento.c_str());
	
	buildDataset(rs,0);
	buildDataset(ta,1);
	buildDataset(hr,2);	
	buildDataset(vv,3);	
}
void Dataset::buildDataset( fstream &file , int  indice )
{
	string line = "";
	int i = 0;
	while(getline(file,line)){
		stringstream strstr(line);
		string word = "";
		while(getline(strstr,word))
			data[i][indice] = atof(word.c_str());
		if(i == num_datos - 1) 
			break;
		i++;
	}
}

//CLASE ALGORITMO GENETICO
class Algoritmo
{
	public:	
		Poblacion poblacion;
		Dataset dataset = Dataset();
		int seleccion[2] = {0,0};
		long double fitness[poblacion_max][2]; //GUARDAR EL FITNESS Y EL NUMERO DEL INDIVIDUO
		
	public:
		Algoritmo(Poblacion);

		void zeros(int vect [poblacion_max]);
		long double exponencial(double , double);
		double pmutacion();

		void evolucion();
		void calcFitness();
		
		long double MSE( vector<double> &individuo );     //se recibe un individuo;
		long double SSE( vector<double> &individuo );	    //se recibe un individuo; 
		long double getFYT( double dataset[num_sen] , vector<double> &individuo );  //se recibe un individuo y el dataset en la posicion t ;
		double error( double , long double ); // se recibe yt y fyt => dato actual y la evaluacion de la funcion fy en el instante t
		
		void reproducir();
		void torneo(int *descendencia);
		void cruza(int *descendencia);

		void combinarGenes(int,int, double [][tam_individuo+1]);
		
		int seleccionarPadre();
		bool padreNoCompentencia( int, long double [][2] );
		int mejorPadreTorneo( long double [][2], int );

		void mutar();
		void poda();

		void ordenarFitnessMenorMayor();

};
Algoritmo::Algoritmo( Poblacion _poblacion )
{
	poblacion = _poblacion;
	dataset.getDataset();
}
void Algoritmo::evolucion()
{
	calcFitness();
	for(int generacion = 0; generacion < num_iteraciones; generacion++)
	{
		cout<<"Generacion No: "<<generacion+1<<endl;
		poblacion.mostrarPoblacion(fitness);
		cout<<endl;
		reproducir();
		mutar();
		poda();
		calcFitness();
		cout<<endl;
	}
}

//METODOS PARA CALCULAR EL FITNESS
void Algoritmo::calcFitness()
{
	for(int i = 0; i < poblacion_act; i++)
	{
		fitness[i][0] = MSE(poblacion.poblacion[i]);
		fitness[i][1] = i;
	}
}
long double Algoritmo::MSE( vector<double> &individuo )
{
	return SSE(individuo);
}
long double Algoritmo::SSE( vector<double> &individuo )
{
	long double sse = 0.0; 
	double fy;
	long double fyt;
	int l = 50; // l = dado por el individuo;
	for(int i = l; i < num_datos; i++){
		fy  = dataset.data[i][0];
		fyt = getFYT(dataset.data[i],individuo);
		sse += error(fy,fyt) / (num_datos - l);
	}
	return sse;
}
long double Algoritmo::getFYT( double dataset[num_sen] , vector<double> &individuo )
{	
	bool endIndividuo = false;
	int topeIndividuo = (tam_individuo-1)/3;
	int rango  = topeIndividuo;
	
	int inData;
	int initial = 0;
	double a    = 0.00;
	long double fyt  = 0.00;
	long double eval = 0.00;

	while( endIndividuo != true )
	{
		inData  = 1;
		a = individuo[initial++];
		eval = exponencial(dataset[inData],individuo[initial++]);
		for ( int i = initial; i < topeIndividuo; i++ )   
			eval *= exponencial(dataset[inData++],individuo[i]);
		
		eval = eval * a;

		fyt = fyt + eval;

		initial = topeIndividuo;

		topeIndividuo += rango; 
		
		if ( topeIndividuo == ( tam_individuo - 1) + rango )
			endIndividuo = true;	
	}
	return fyt;
}
long double Algoritmo::exponencial(double xi,double gen)
{
	return pow(xi,gen);
}
double Algoritmo::error(double yt, long double fyt)
{
	return pow((yt - fyt),2);
} 

//METODOS PARA LA REPRODUCCION (SELECCION POR TORNEO Y CRUZA)
void Algoritmo::reproducir()
{
	//SELECCION POR TORNEO
	int descendencia[poblacion_max];
	torneo(descendencia);

	//HACER CRUZA
	cruza(descendencia);
}
void Algoritmo::torneo(int *descendencia)
{	
	int k = 3;
	long double competencia[k][2];

	zeros(descendencia);

	int competidores = 0;
	while ( competidores <  poblacion_act )
	{
		int i = 0;
		while ( i < k )
		{
			int padre = seleccionarPadre();
			if(padreNoCompentencia(padre,competencia) == false)
			{
				competencia[i][0] = fitness[padre][0];
				competencia[i][1] = padre;
				i++;
			}		
		}
		descendencia[mejorPadreTorneo(competencia,k)] += 1; 
		competidores ++;
	}
}
void Algoritmo::zeros( int vect[poblacion_max] )
{
	for ( int i = 0; i < poblacion_max; i++ )
		vect[i] = 0;
}
int Algoritmo::seleccionarPadre()
{
	return rand() % poblacion_act;
}
bool Algoritmo::padreNoCompentencia(int padre, long double competencia[][2])
{
	for ( int i = 0; i < poblacion_act; i++ )
		if(padre == competencia[i][1])
			return true;
	return false;
}
int Algoritmo::mejorPadreTorneo( long double competencia[][2], int k )
{
	double fitnessMin = competencia[0][0];
	int padre         = competencia[0][1];

	for ( int i = 0; i < k; i++ )
	{
		if(fitnessMin > competencia[i][0] )
		{
			fitnessMin = competencia[i][0];
			padre      = competencia[i][1];
		}
	}
	return padre;
}
void Algoritmo::cruza(int *descendencia)
{
	double mcruza[poblacion_act][tam_individuo+1];
	int icruza = 0;
	for( int i = 0; i < poblacion_act; i++ )
		if( descendencia[i] != 0)
			for ( int j = 0; j < descendencia[i]; j++ )
			{
				for ( int k = 0; k < tam_individuo; k++ )
					mcruza[icruza][k] = poblacion.poblacion[i][k];
				mcruza[icruza][tam_individuo] = i;
				icruza++;
			}

	//seleccionar dos padres dentro de la matriz de cruza  que no sean del  mismo padre
	for ( int i = 0; i < poblacion_act; i++ )
	{
		int padreA = (rand()%poblacion_act); // seleccionar al padre inicial
		int padreB;
		do{
			padreB = (rand()%poblacion_act);
		}while(mcruza[padreA][tam_individuo] == mcruza[padreB][tam_individuo]);

		combinarGenes(padreA,padreB,mcruza);

		poblacion_act_des++;
	}

}
void Algoritmo::combinarGenes( int padreA, int padreB,double mcruza[][tam_individuo+1])
{
	int puntoCruza = rand()%tam_individuo;
	for ( int i = 0; i < puntoCruza; i++ )
		poblacion.poblacion[poblacion_act_des][i] = mcruza[padreA][i];
	
	for ( int i = puntoCruza; i < tam_individuo; i++ )
		poblacion.poblacion[poblacion_act_des][i] = mcruza[padreB][i];

	poblacion_act_des++;

	for ( int i = 0; i < puntoCruza; i++ )
		poblacion.poblacion[poblacion_act_des][i] = mcruza[padreB][i];

	for ( int i = puntoCruza; i < tam_individuo; i++ )
		poblacion.poblacion[poblacion_act_des][i] = mcruza[padreA][i];
}

//METODOS PARA LA MUTACIÓN
void Algoritmo::mutar()
{
	for ( int i = poblacion_act ; i < poblacion_act_des; i++)
	{
		int  intercambios = (rand()%tam_individuo/2);
		for ( int j = 0; j < intercambios; j++ )
		{
			int posA = rand() % tam_individuo;
			int posB = rand() % tam_individuo;
			double genTemp = poblacion.poblacion[i][posA];
			poblacion.poblacion[i][posA] = poblacion.poblacion[i][posB];
			poblacion.poblacion[i][posB] = genTemp;
 		}
	}
}

void Algoritmo::poda()
{
	//ordenar de menor a mayor el fitness
	ordenarFitnessMenorMayor();

	//ordenar poblacion conforme al fitness
}
void Algoritmo::ordenarFitnessMenorMayor()
{
	for ( int i = 0; i < poblacion_act; i++ )
	{
		for ( int j = i+1; j < poblacion_act; j++ )
		{
			if ( fitness[j][0] < fitness[i][0] )
			{
				long double fitnessAux = fitness[i][0];
				int individuoPosAux    = fitness[i][1];
				fitness[i][0] = fitness[j][0];
				fitness[i][1] = fitness[j][1];
				fitness[j][0] =  fitnessAux;
				fitness[j][1] =  individuoPosAux;
			}
		}
	}
}
int main(){
	/*while(true)
		cout<<fixed<<setprecision(3)<<(double)(rand()*10001/(RAND_MAX-1))/10000<<endl;*/
	Poblacion poblacion = Poblacion();
	poblacion.poblar();
	
	Algoritmo algoritmo = Algoritmo(poblacion);
	algoritmo.evolucion();
	
	return 0;
}

